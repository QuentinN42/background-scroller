from selenium.webdriver import Firefox

from front.routes import make_server


def main(bw):
    make_server(bw)


if __name__ == "__main__":
    browser = Firefox(executable_path="geckodriver")
    main(browser)
    browser.quit()
