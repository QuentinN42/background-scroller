from functools import partial
from math import floor, ceil as top


def get_link(i, bw):
    """
    Get one link in one page.
    """
    xpath = f"/html/body/div[5]/div[2]/div/div[1]/div[3]/div[2]/ul/li[{i}]"
    return bw.find_element_by_xpath(xpath).find_element_by_tag_name("a").get_property("href")


def get_links_in_page(page, bw, start=1, stop=18):
    """
    Get all links in one page.
    """
    print(f"page {page}, {start} -> {stop}")
    bw.get(f"http://wallpaperswide.com/page/{page}")
    return list(map(partial(get_link, bw=bw), range(start, stop + 1)))


def get_links(bw, start, stop):
    """
    Ask the games within the good pages.

    3 steps :
        page 1 : 1 -> [start -> page end]
        page 2 -> n-1 : [1 -> page end]
        page n : [1 -> stop] -> page end
    """
    print("Getting all links...")
    
    # calculate pages id (each page has 18 elements)
    n_start = max(floor(start / 18), 1)
    n_stop = top(stop / 18)
    
    if n_stop != n_start:
        # incrementing res to get all desired links
        res = get_links_in_page(n_start, bw, start=max(start % 18, 1))
        for n in range(n_start + 1, n_stop):
            res += get_links_in_page(n, bw)
        res += get_links_in_page(n_stop, bw, stop=stop % 18)
    else:
        res = get_links_in_page(n_start, bw, start=max(start % 18, 1), stop=stop % 18)
    
    return res
