from .scrapper import get_links


def to_json(link):
    """
    Return a json with the link and the image.

    >>> to_json("http://wallpaperswide.com/hogwarts_legacy-wallpapers.html")
    {
        "url": "http://wallpaperswide.com/hogwarts_legacy-wallpapers.html",
        "preview": "http://hd.wallpaperswide.com/thumbs/hogwarts_legacy-t2.jpg",
        "download": "http://wallpaperswide.com/download/hogwarts_legacy-wallpaper-1920x1080.jpg"
    }
    """
    return {
        "url": link,
        "preview": "http://hd." + link[7:26] + "thumbs/" + link[26:-15] + "t2.jpg",
        "download": link[:26] + "download/" + link[26:-6] + "-1920x1080.jpg"
    }


def wallpapers(bw, start, stop):
    """
    Gives you all the wallpapers in range [start, stop].
    """
    links = get_links(bw, start, stop)
    return list(map(to_json, links))
