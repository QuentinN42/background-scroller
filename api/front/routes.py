from os import getenv

from flask import Flask
from flask_restful import Api

from flask_restful import Resource
from flask_restful.reqparse import RequestParser

from .wallpapers import wallpapers

parser = RequestParser(bundle_errors=True)
parser.add_argument("start", type=int, required=True)
parser.add_argument("stop", type=int, required=True)


def make_server(bw):
    class Main(Resource):
        def get(self):
            args = parser.parse_args()
            return wallpapers(bw, **args)
    
    app = Flask(__name__)
    api = Api(app)
    
    api.add_resource(Main, '/')
    
    app.run(host=getenv("HOST"), port=getenv("PORT"))
